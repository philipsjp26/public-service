FROM golang:1.20
ARG USERNAME=${USERNAME}
ARG PASSWORD=${PAT}
ARG GO_BUILD_COMMAND="go build -tags static_all"

WORKDIR /app
ADD . .
ENV GOPRIVATE=gitlab.com/philipsjp26/private-proto
RUN printf "machine gitlab.com login $USERNAME password $PASSWORD" > /root/.netrc
RUN git config --global --add url."https://gitlab.com/philipsjp26".insteadOf "https://gitlab.com/philipsjp26"

CMD echo $(cat /root/.netrc)
RUN go mod tidy && go mod download && go mod vendor
RUN eval $GO_BUILD_COMMAND

